# `strait-alpha`

> Part of [Chasten](https://chasten.gitlab.io).

`strait-alpha` is currently in alpha and makes no promises about API stability. It will eventually be superceded by `strait` (without the alpha), which will be a new project, a new NPMJS package, and a new git repository.

## Installing / Getting started

To use `strait-alpha` you'll need the latest [Node.js](https://nodejs.org) LTS version or newer and a Node.js package manager of your choosing. The examples all assume `npm`, but the alternatives work just as well.

Here's a snippet you can copy/paste into Bash to get a working example. You can safely run this in any empty directory.

```shell
npm init -y
npm install @chasten/strait-alpha

cat <<'EOF' >> example.js
const http = require('http');
const { requestListener } = require('@chasten/strait-alpha');
const handler = request => ({
  "body": JSON.stringify(request),
  "headers": { "content-type": "application/json" },
  "status": 200,
});
http.createServer(requestListener(handler)).listen(3000);
EOF

node example.js&
curl http://localhost:3000
```

You'll now have a new file called example.js, you'll have a running node process listening on port 3000, and you'll have sent a cURL request to see what your web service responds.

## Developing

If you'd like to develop `strait-alpha` on your local machine you can get started pretty easily:

```shell
git clone https://gitlab.com/chasten/strait-alpha.git
cd strait-alpha/
npm ci
```

This will clone the repository from gitlab, change directory, and install the required development dependencies.

### Deploying / Publishing

In order to publish new versions of `strait-alpha` you'll need public permissions for the organization on NPMJS and you'll need to be logged in with `npm` when you run the following command:

```shell
npm publish
```

This publishes a version (specified in the `version` field in `package.json`) to NPMJS.com.

## Features

`strait-alpha` provides simple utility functions that allow you to develop Node.js web services using functional programming.

`strait-alpha` is:

-   intended to work *with* the somewhat low level Node.js functions such as `http.createServer` rather than to replace them
-   surprisingly heavy on concepts given its short code length - see https://chasten.gitlab.io
-   probably a departure from the Node.js web frameworks you know
-   designed so that *you* wire a bunch of simple parts together to create a functioning whole

`strait-alpha` is **not**:

-   concerned with routing (see `chasten/whither` for that)
-   concerned with effects and coeffects (see `chasten/prithee` for that)
-   a full-blown web framework (we have plenty already)
-   an out-of-the-box anything; you'll need to some work

## Contributing

`strait-alpha` happily accepts contributions. Contributing to the project has a few requirements:

-   The rules outlined in CONTRIBUTING.md must be followed.
-   The ESLint style guide (@chasten/eslint-config) must be followed.

The latter should be set up in your editor/IDE of choice

## Links

- Project homepage: https://chasten.gitlab.io
- Repository: https://gitlab.com/chasten/strait-alpha
- Issue tracker: https://gitlab.com/chasten/strait-alpha/issues
  - In case of sensitive bugs like security vulnerabilities, please create a [confidential issue](https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html) on GitLab. We value your effort
    to improve the security and privacy of this project!
- Related projects:
  - [`whither-alpha`](https://gitlab.com/chasten/whither-alpha)

## Licensing

The code in this project is licensed under MIT license.
