const sinon = require("sinon");
const { http2 } = require("../index");

it("sends a 500 if the handler throws an error", done => {
  const respondMock = jest.fn();
  const handler = () => {
    throw new Error();
  };
  const listener = http2(handler);
  listener({
    "end": sinon.stub(),
    "on": (type, callback) => {
      if (type === "end") {
        callback();
      }
    },
    "respond": respondMock,
  }, {});

  setImmediate(() => {
    expect(respondMock).toHaveBeenCalledWith({
      ":status": 500,
    });
    done();
  });
});

it("defaults to 500 if unspecified by handler", done => {
  const respondMock = jest.fn();
  const handler = () => ({});
  const listener = http2(handler);
  listener({
    "end": sinon.stub(),
    "on": (type, callback) => {
      if (type === "end") {
        callback();
      }
    },
    "respond": respondMock,
  }, {});

  setImmediate(() => {
    expect(respondMock).toHaveBeenCalledWith({
      ":status": 500,
    });
    done();
  });
});

it("passes the body to the handler", done => {
  const handler = jest.fn()
    .mockReturnValue({ "status": 200 });
  const listener = http2(handler);
  listener({
    "end": sinon.stub(),
    "on": (type, callback) => {
      if (type === "data") {
        callback(Buffer.from("request body"));
      }
      if (type === "end") {
        callback();
      }
    },
    "respond": sinon.stub(),
  }, {});
  setImmediate(() => {
    expect(handler).toBeCalledWith(expect.objectContaining({
      "body": expect.any(String),
    }));
    done();
  });
});

it("passes the headers to the handler", done => {
  const handler = jest.fn()
    .mockReturnValue({ "status": 200 });
  const listener = http2(handler);
  listener({
    "end": sinon.stub(),
    "on": (type, callback) => {
      if (type === "data") {
        callback(Buffer.from("request body"));
      }
      if (type === "end") {
        callback();
      }
    },
    "respond": sinon.stub(),
  }, {
    "content-type": "application/json",
  });
  setImmediate(() => {
    expect(handler).toBeCalledWith(expect.objectContaining({
      "headers": {
        "content-type": "application/json",
      },
    }));
    done();
  });
});

it("doesn't pass the special HTTP2 headers to the handler", done => {
  const handler = jest.fn()
    .mockReturnValue({ "status": 200 });
  const listener = http2(handler);
  listener({
    "end": sinon.stub(),
    "on": (type, callback) => {
      if (type === "data") {
        callback(Buffer.from("request body"));
      }
      if (type === "end") {
        callback();
      }
    },
    "respond": sinon.stub(),
  }, {
    ":method": "POST",
    "content-type": "application/json",
  });
  setImmediate(() => {
    expect(handler).toBeCalledWith(expect.objectContaining({
      "headers": {
        "content-type": "application/json",
      },
    }));
    done();
  });
});
