const { wrap } = require("../index");

it("wraps a sync handler without middlewares", async () => {
  const handler = () => ({ "status": 200 });
  const wrapped = wrap(handler);
  expect(await wrapped({})).toEqual({ "status": 200 });
});

it("wraps a sync handler with one middleware", async () => {
  const handler = () => ({ "status": 200 });
  const middleware1 = h => req => ({ ...h(req), "body": "response" });
  const wrapped = wrap(handler, [middleware1]);
  expect(await wrapped({})).toEqual({ "body": "response", "status": 200 });
});

it("wraps a sync handler with two middlewares", async () => {
  const handler = () => ({ "status": 200 });
  const middleware1 = h => req => ({ ...h(req), "body": "response" });
  const middleware2 = h => req => h({ ...req, "body": "request" });
  const wrapped = wrap(handler, [middleware1, middleware2]);
  expect(await wrapped({})).toEqual({ "body": "response", "status": 200 });
});

it("wraps an async handler without middlewares", async () => {
  const handler = async () => {
    const val = await Promise.resolve({ "status": 200 });
    return val;
  };
  const wrapped = wrap(handler);
  expect(await wrapped({})).toEqual({ "status": 200 });
});

it("wraps an async handler with one middleware", async () => {
  const handler = async () => {
    const val = await Promise.resolve({ "status": 200 });
    return val;
  };
  const middleware = h => async req => {
    const response = { ...await h(req), "body": "response" };
    return response;
  };
  const wrapped = wrap(handler, [middleware]);
  expect(await wrapped({})).toEqual({ "body": "response", "status": 200 });
});

it("wraps an async handler with two middlewares", async () => {
  const handler = async () => {
    const val = await Promise.resolve({ "status": 200 });
    return val;
  };
  const middleware1 = h => async req => {
    const response = { ...await h(req), "body": "response" };
    return response;
  };
  const middleware2 = h => req => h({ ...req, "body": "request" });
  const wrapped = wrap(handler, [middleware1, middleware2]);
  expect(await wrapped({})).toEqual({ "body": "response", "status": 200 });
});
