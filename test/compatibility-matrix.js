/**
 * This file exists to ensure the implementation of `wrap` works with any
 * combination of handlers and middlewares, sync or async. It ensures a number
 * of things:
 *
 * - The handler is applied (setting status: 200)
 * - The first middleware is applied (setting localPort: 1337)
 * - The second middleware is applied (setting some headers)
 * - The middlewares run in the expected order:
 *   First middleware sets localPort: 1337.
 *   Second middleware sets body: localPort.
 *   The result should have body: 1337.
 *
 * Any failure will be printed and generate a non-zero exit code.
 */

/* eslint-disable no-unused-vars,require-await */

const assert = require("assert");
const { wrap } = require("../");

function hSync (request) {
  return {
    "status": 200,
  };
}

async function hAsync (request) {
  return {
    "status": 200,
  };
}

function m1SyncSync (handler) {
  return request => handler({
    ...request,
    "localPort": 1337,
  });
}

function m1SyncAsync (handler) {
  return async request => handler({
    ...request,
    "localPort": 1337,
  });
}

async function m1AsyncSync (handler) {
  return request => handler({
    ...request,
    "localPort": 1337,
  });
}

async function m1AsyncAsync (handler) {
  return async request => handler({
    ...request,
    "localPort": 1337,
  });
}

function m2Sync (handler) {
  return async request => {
    const response = await handler(request);
    return {
      ...response,
      "body": request.localPort,
      "headers": {
        "content-type": "application/json",
      },
    };
  };
}

async function m2Async (handler) {
  return async request => {
    const response = await handler(request);
    return {
      ...response,
      "body": request.localPort,
      "headers": {
        "content-type": "application/json",
      },
    };
  };
}

const handlers = [hSync, hAsync];
const middlewares1 = [m1SyncSync, m1SyncAsync, m1AsyncSync, m1AsyncAsync];
const middlewares2 = [m2Sync, m2Async];

async function test () {
  for (const handler of handlers) {
    for (const middleware1 of middlewares1) {
      for (const middleware2 of middlewares2) {
        try {
          const wrapped = wrap(handler, [middleware1, middleware2]);
          const ringRequest = {
            "body": "",
            "headers": {},
            "url": "/",
          };
          const answer = await wrapped(ringRequest);
          assert.deepEqual(answer, {
            "body": 1337,
            "headers": {
              "content-type": "application/json",
            },
            "status": 200,
          });
          console.log(
            "✔\t",
            [handler.name, middleware1.name, middleware2.name],
          );
        } catch (error) {
          process.exitCode = 1;
          console.error(
            "❌\t",
            [handler.name, middleware1.name, middleware2.name],
            error.message,
          );
        }
      }
    }
  }
}

test()
  .then(console.log);
