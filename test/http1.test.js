const { http1 } = require("../index");

it("sends a 500 if the handler throws an error", done => {
  const writeHeadMock = jest.fn();
  const handler = () => {
    throw new Error();
  };
  const listener = http1(handler);
  listener({
    on (type, callback) {
      if (type === "data") {
        callback(Buffer.from("request"));
      }
      if (type === "end") {
        callback();
      }
    },
    "socket": {},
  }, {
    "end": jest.fn(),
    "writeHead": writeHeadMock,
  });

  setImmediate(() => {
    expect(writeHeadMock).toHaveBeenCalledWith(500, {});
    done();
  });
});

it("sends a 500 if the handler returns a rejected promise", done => {
  const writeHeadMock = jest.fn();
  const handler = async () => {
    await Promise.reject(new Error());
  };
  const listener = http1(handler);
  listener({
    on (type, callback) {
      if (type === "data") {
        callback(Buffer.from("request"));
      }
      if (type === "end") {
        callback();
      }
    },
    "socket": {},
  }, {
    "end": jest.fn(),
    "writeHead": writeHeadMock,
  });

  setImmediate(() => {
    expect(writeHeadMock).toHaveBeenCalledWith(500, {});
    done();
  });
});

it("sends a 500 if the handler doesn't return a status", done => {
  const writeHeadMock = jest.fn();
  const handler = () => ({});
  const listener = http1(handler);
  listener({
    on (type, callback) {
      if (type === "data") {
        callback(Buffer.from("request"));
      }
      if (type === "end") {
        callback();
      }
    },
    "socket": {},
  }, {
    "end": jest.fn(),
    "writeHead": writeHeadMock,
  });

  setImmediate(() => {
    expect(writeHeadMock).toHaveBeenCalledWith(500, {});
    done();
  });
});

it("calls the underlying node.js functions for sending a response", done => {
  const endMock = jest.fn();
  const handler = jest.fn(() => ({ "body": "response", "status": 200 }));
  const listener = http1(handler);
  const writeHeadMock = jest.fn();
  listener({
    on (type, callback) {
      if (type === "data") {
        callback(Buffer.from("request"));
      }
      if (type === "end") {
        callback();
      }
    },
    "socket": {},
  }, {
    "end": endMock,
    "writeHead": writeHeadMock,
  });

  setImmediate(() => {
    expect(writeHeadMock).toHaveBeenCalledWith(200, {});
    expect(endMock).toHaveBeenCalledWith("response", "utf8");
    done();
  });
});

it("passes along body, headers, and status from handler", done => {
  const endMock = jest.fn();
  const handler = jest.fn(() => ({
    "body": "response",
    "headers": {
      "X-Header": "header",
    },
    "status": 201,
  }));
  const listener = http1(handler);
  const writeHeadMock = jest.fn();
  listener({
    on (type, callback) {
      if (type === "data") {
        callback(Buffer.from("request"));
      }
      if (type === "end") {
        callback();
      }
    },
    "socket": {},
  }, {
    "end": endMock,
    "writeHead": writeHeadMock,
  });

  setImmediate(() => {
    expect(writeHeadMock)
      .toHaveBeenCalledWith(201, { "X-Header": "header" });
    expect(endMock)
      .toHaveBeenCalledWith("response", "utf8");
    done();
  });
});
