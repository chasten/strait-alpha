const { Readable } = require("stream");

function http1 (handler) {
  return (request, response) => {
    const chunks = [];

    request.on("data", chunk => {
      chunks.push(chunk);
    });

    request.on("end", () => {
      const ringRequest = {
        "body": Buffer.concat(chunks).toString(),
        "headers": request.headers,
        "method": request.method,
        "url": request.url,
      };

      new Promise(resolve => {
        resolve(handler(ringRequest));
      })
        .catch(() => ({ "status": 500 }))
        .then(({ body = "", headers = {}, status = 500 }) => {
          response.writeHead(status, headers);
          if (body instanceof Readable) {
            body.pipe(response);
            body.on("end", response.end);
          } else {
            response.end(body, "utf8");
          }
        });
    });
  };
}

function http2 (handler) {
  return (stream, http2Headers) => {
    const chunks = [];

    stream.on("data", chunk => {
      chunks.push(chunk);
    });

    stream.on("end", () => {
      const headers = Object.entries(http2Headers).reduce((o, [k, v]) => {
        if (!k.startsWith(":")) {
          o[k] = v;
        }
        return o;
      }, {});
      const ringRequest = {
        "body": Buffer.concat(chunks).toString(),
        headers,
        "method": http2Headers[":method"],
        "url": http2Headers[":path"],
      };

      new Promise(resolve => {
        resolve(handler(ringRequest));
      })
        .catch(() => ({ "status": 500 }))
        .then(({ body = "", "headers": responseHeaders = {}, status = 500 }) => {
          stream.respond({
            ...responseHeaders,
            ":status": status,
          });

          if (body instanceof Readable) {
            body.pipe(stream);
            body.on("end", stream.end);
          } else {
            stream.end(body, "utf8");
          }
        });
    });
  };
}

function wrap (handler, middlewares = []) {
  let wrapper = Promise.resolve(handler);
  for (const middleware of middlewares.reverse()) {
    wrapper = wrapper.then(h => middleware(h));
  }
  return request => wrapper.then(f => f(request));
}

module.exports = {
  http1,
  http2,
  wrap,
};
